import React from 'react';
import packageJSON from '../../package.json';

const classes = {
  root: {
    width: 100,
    height: 100,
    position: 'fixed',
    top: 0, left: 0,
    color: '#fff',
    zIndex: 9999,
  },
  textDev: {
    display: 'block',
    transform: 'rotate(-45deg)',
    background: '#000',
    position: 'absolute',
    width: '150%',
    textAlign : 'center',
    top: 28,
    left: '-34px',
    boxShadow: '0 0px 5px #00000094',
  },
  version: {
    top: '-10px',
    left: '-56px',
    color: 'white',
    display: 'block',
    transform: 'rotate(-45deg)',
    background: 'red',
    textAlign: 'center',
    position: 'absolute',
    padding: '20px',
    width: '150%',
  }
};

export function DevMark() {
  return(
    <div style={classes.root}>
      <span style={classes.version}>{packageJSON.version}</span>
      <span style={classes.textDev}>Development</span>
    </div>
  );
}