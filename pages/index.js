import {useState} from 'react';
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import dayjs from 'dayjs';
import Head from 'next/head';
import LinearProgress from '@material-ui/core/LinearProgress';
import styles from './Home.module.css'
const NumberFormat = dynamic(() => import('react-number-format'), { ssr: false });

const MIN_PAYMENT = 300000;
const MAX_PAYMENT = 16000000;

export const MerchantName = 0;
export const MerchantId = 1;
export const MerchantKey = 2;

export const merchantList = [
  ["Shopee","e7a591f9-837f-4583-8dac-69065f8ac0c6","eyJhbGciOiJIUzI1NiJ9.MQ.dzwnmTfB4xYRn7K7mFwBE1vVOIYVVvg0IrMITJsqEfM"],
  ["Decathlon","fadfe636-5a4d-463c-ad93-3e530bbf3e10","eyJhbGciOiJIUzI1NiJ9.MTA.vruqwJGeyR8VaiBbn1XvI4kYP5BWWsLf5QScPTaagYI"],
  ["Takashimaya","8b3ed786-2e6c-4ec8-8bf5-e3f43ed3f875","eyJhbGciOiJIUzI1NiJ9.MTE.zuj6NGUfUtqC_0o73HKaRENc-LVYMJdfWymerH3u9XI"],
];

export default function Home() {
  const [loading, setLoading] = useState(false);
  const [amount, setAmount] = useState('300000');
  const [merchant, setMerchant] = useState(0);
  const [dateTime, setDatetime] = useState(new Date());
  const router = useRouter();
  const staticData = {
    deliveryDate: dayjs().format('YYYY-MM-DD'),
    requestId: `${Number(dateTime)}`,
    orderId: `OD_${Number(dateTime)}`,
    notifyUrl: '',
    returnUrl: '',
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    console.log(process.env.DEV_MODE)
    fetch('/api/purchase', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
          amount: parseInt(amount),
          ...staticData,
          notifyUrl: window.location.origin +  '/api-hook',
          returnUrl: window.location.origin + '/checkout',
          merchant,
        })
      })
      .then((resp) => {
        resp.json().then(({payUrl}) => {
          if (payUrl) {
            router.replace(payUrl);
          }
        });
        setDatetime(new Date());
      })
      .catch(e => {})
      .finally(() => setLoading(false));
  };

  const validAmount = amount < MIN_PAYMENT || amount > MAX_PAYMENT;

  return (
    <div className={styles.container}>
      <Head>
        <title>Merchant Create Purchase</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <div className={styles.grid}>
          <div className={styles.card}>
            <h3>Create Purchase</h3>
            <form name='purchase_create' onSubmit={handleSubmit}>
              <div className={styles.hiddenValue}>
                <ul>
                  <li>
                    deliveryDate: {staticData.deliveryDate}
                    <input type='hidden' name='deliveryDate' value={staticData.deliveryDate} />
                  </li>
                  <li>
                    requestId: {staticData.requestId}
                    <input type='hidden' name='requestId' value={staticData.requestId} />
                  </li>
                  <li>
                    orderId: {staticData.orderId}
                    <input type='hidden' name='orderId' value={staticData.orderId} />
                  </li>
                </ul>
              </div>
              <div style={{display: 'flex', marginBottom: 10}}>
                <label>Merchant</label>
                <div>
                  <select name='merchant' style={{marginLeft: 28}} onChange={(e) => setMerchant(e.target.value)}>
                      {merchantList.map((item, index) => <option key={item[MerchantId]} value={index}>{item[MerchantName]}</option>)}
                    </select>
                </div>
              </div>
              <div style={{display: 'flex', justifyContent: 'space-between'}}>
                <label>Amount</label>
                <div>
                  <NumberFormat onValueChange={vObj => setAmount(vObj.value)} value={amount} decimalScale={0} thousandSeparator={true} name='amount'/>
                  <span>&nbsp;VND</span>
                </div> 
                <button disabled={loading || validAmount}>Submit &rarr;</button>
              </div>
              {validAmount && 
                <div style={{fontSize: 10, opacity: 0.5}}>
                  Amount should be greater than {MIN_PAYMENT.toLocaleString()}VND and lower than {MAX_PAYMENT.toLocaleString()}VND</div>
              }
            </form>
            {loading && <LinearProgress style={{position: 'absolute', bottom: 0, right: 0, width: '100%'}} />}
          </div>

        </div>
      </main>
    </div>
  )

}