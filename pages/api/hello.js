// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default (req, res) => {
  res.statusCode = 200
  res.json({ name: 'John Doe' })
}

/**
 * clientId: '79465589-8cce-47a9-ab56-6faab82556e1',
    authToken: 'eyJhbGciOiJIUzI1NiJ9.MQ.dzwnmTfB4xYRn7K7mFwBE1vVOIYVVvg0IrMITJsqEfM'
 */