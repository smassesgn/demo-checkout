const {CLIENT_ID, AUTH, SIGNATURE, BASE_URL, DEV_MODE} = process.env;
const clientId= CLIENT_ID || '';
const authToken= AUTH || '';
const signature= SIGNATURE || '';
const requestCreateAPI= BASE_URL + '/me/pr';

import {merchantList, MerchantId, MerchantName, MerchantKey} from '../index';

export default async (req, res) => {
  const {
    method,
    body
  } = req;
  const merchant = merchantList[body.merchant];
  console.log(merchant)
  // res.statusCode = 200;
  // res.json(merchant);
  try {
    console.log(BASE_URL, requestCreateAPI);
    const resp = await fetch(requestCreateAPI, {
      method: method,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': merchant[MerchantKey]
      },
      body: JSON.stringify({...body, signature, clientId: merchant[MerchantId]})
    })
    const json = await resp.json();
    res.statusCode = resp.status;
    res.json(json);
    // res.statusCode = 200;
    // res.json({ok: 1});
  } catch (e) {
    console.log(e);
    const { response, status } = e;
    res.statusCode = status;
    res.json(response)
  }
}
