import Head from 'next/head';
import { useRouter } from 'next/router';
import Link from 'next/link'
import styles from '../styles/Home.module.css'

export default function Checkout() {
  const route = useRouter();
  const {status} = route.query;
  return (
    <div className={styles.container}>
      <Head>
        <title>Merchant Checkout Status</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <div className={styles.grid}>
          <div className={styles.card}>
            <h3>
              Checkout {status === 'true' ? 'success' : 'failed'}
            </h3>
            <Link href="/">Continue Shopping</Link>
          </div>

        </div>
      </main>
    </div>
  )
}
