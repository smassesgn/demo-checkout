import Document, { Html, Head, Main, NextScript } from 'next/document'
import { DevMark } from '../Components/DevMark';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }
  
  render() {
    const { DEV_MODE } = process.env;
    return (
      <Html>
        <Head>
          <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
          <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
        </Head>
        <body>
          {DEV_MODE === '1' && <DevMark />}
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
