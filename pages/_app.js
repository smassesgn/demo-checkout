// pages/_app.js
import '../styles.css'
import styles from '../styles/Home.module.css'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
