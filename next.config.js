module.exports = {
  compress: true,
  poweredByHeader: false,
  generateEtags: false,
}